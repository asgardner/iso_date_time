class IsoDateTime extends DateTime {
  IsoDateTime(int year,
      [int month = 1,
      int day = 1,
      int hour = 0,
      int minute = 0,
      int second = 0,
      int millisecond = 0,
      int microsecond = 0])
      : super(
            year, month, day, hour, minute, second, millisecond, microsecond) {
    update();
  }

  IsoDateTime.utc(int year,
      [int month = 1,
      int day = 1,
      int hour = 0,
      int minute = 0,
      int second = 0,
      int millisecond = 0,
      int microsecond = 0])
      : super.utc(
            year, month, day, hour, minute, second, millisecond, microsecond) {
    update();
  }

  IsoDateTime.fromMicroSecondsSinceEpoch(int microsecondsSinceEpoch,
      {bool isUtc = false})
      : super.fromMicrosecondsSinceEpoch(microsecondsSinceEpoch, isUtc: isUtc) {
    update();
  }

  IsoDateTime.fromMillisecondsSinceEpoch(int millisecondsSinceEpoch,
      {bool isUtc = false})
      : super.fromMillisecondsSinceEpoch(millisecondsSinceEpoch, isUtc: isUtc) {
    update();
  }

  IsoDateTime.now() : super.now() {
    update();
  }

  IsoDateTime.timestamp()
      : super.fromMicrosecondsSinceEpoch(DateTime.now().microsecondsSinceEpoch,
            isUtc: true) {
    update();
  }

  IsoDateTime.fromDateTime(DateTime dateTime)
      : super(
            dateTime.year,
            dateTime.month,
            dateTime.day,
            dateTime.hour,
            dateTime.minute,
            dateTime.second,
            dateTime.millisecond,
            dateTime.microsecond) {
    update();
  }

  IsoDateTime.fromDateTimeUtc(DateTime dateTime)
      : super.utc(
            dateTime.year,
            dateTime.month,
            dateTime.day,
            dateTime.hour,
            dateTime.minute,
            dateTime.second,
            dateTime.millisecond,
            dateTime.microsecond) {
    update();
  }

  late int dayOfYear, weekOfYear;

  void update() {
    // Days since January 1 is the day of year, but +1 bc there is no day zero.
    dayOfYear = toUtc().difference(DateTime.utc(year)).inDays + 1;

    // If only weeks of year were so easy.
    var jan1 = DateTime(year, 1, 1); // Get the first day of the current year.

    if (jan1.weekday <= DateTime.thursday) {
      // If the first day of the year was Monday...Thursday, everything is relatively simple.
      // Add an offset and sub 1 so that we roll over to the second and subsequent weeks at the right time.
      int offsetDOY = (jan1.weekday - DateTime.monday) + dayOfYear - 1;
      // Then, it's just integer division plus one.
      weekOfYear = (offsetDOY ~/ 7) + 1;
    } else {
      // If the first day of the year was Friday...Sunday, those days were in the last
      // week of the previous year. As long as we aren't one of those days, then
      // nothing terrible happens. If you're in one of those days, we have to figure
      // out how many weeks there were in the previous year so that we can figure
      // out the week number, because it could be 52 or 53.

      // Let's see if this is easy.
      bool easy1 = jan1.weekday == DateTime.friday && dayOfYear > 3;
      bool easy2 = jan1.weekday == DateTime.saturday && dayOfYear > 2;
      bool easy3 = jan1.weekday == DateTime.sunday && dayOfYear > 1;
      if (easy1 || easy2 || easy3) {
        int offsetDOY = jan1.weekday - 8 + dayOfYear - 1;
        // Then, it's just integer division plus one.
        weekOfYear = (offsetDOY ~/ 7) + 1;
      } else {
        // So, now for the weird part. We are talking about a day that is in the
        // last week of the previous year. Most years have 52 weeks, but some
        // have 53. 53 week years occur in two situations: (1) when January 1
        // is on a Thursday; and, (2) when January 1 is a Wednesday _and_ the
        // year is a leap year.
        weekOfYear = 52;

        var prevJan1 = DateTime(year - 1, 1, 1);
        bool thurs = prevJan1.weekday == DateTime.thursday;
        bool wed = prevJan1.weekday == DateTime.wednesday;
        bool isDivBy4 = (year - 1) % 4 == 0;
        bool isDivBy100 = (year - 1) % 100 == 0;
        bool isDivBy400 = (year - 1) % 400 == 0;
        bool isLeapYear =
            (isDivBy4 && !isDivBy100) || (isDivBy4 && isDivBy100 && isDivBy400);
        if (thurs || (wed && isLeapYear)) {
          weekOfYear = 53;
        }
      }
    }
  }

  @override
  IsoDateTime add(Duration duration) {
    return isUtc
        ? IsoDateTime.fromDateTimeUtc(super.add(duration))
        : IsoDateTime.fromDateTime(super.add(duration));
  }

  @override
  IsoDateTime subtract(Duration duration) {
    return isUtc
        ? IsoDateTime.fromDateTimeUtc(super.subtract(duration))
        : IsoDateTime.fromDateTime(super.subtract(duration));
  }
}
