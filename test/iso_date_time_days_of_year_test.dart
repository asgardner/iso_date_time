import 'package:iso_date_time/iso_date_time.dart';
import 'package:test/test.dart';

void main() {
  group('ISO 8601 tests', () {
    final idtUtc = IsoDateTime.utc(2023);
    final idt = IsoDateTime(2023);

    setUp(() {});

    test('days of year', () {
      // Day of year after ctor.
      expect(idtUtc.dayOfYear, 1);
      expect(idt.dayOfYear, 1);

      // Add days.
      expect(idtUtc.add(Duration(days: 37)).dayOfYear, 38);
      expect(idt.add(Duration(days: 42)).dayOfYear, 43);

      // Specific days.
      expect(
          IsoDateTime(2023, 12, 31).subtract(Duration(days: 1)).dayOfYear, 364);
      expect(IsoDateTime.utc(2023, 8, 31).subtract(Duration(days: 3)).dayOfYear,
          240);

      // Subtract days.
      expect(IsoDateTime(2023, 6, 7).dayOfYear, 158);
      expect(IsoDateTime.utc(2023, 8, 31).dayOfYear, 243);
    });
  });
}
