import 'package:iso_date_time/iso_date_time.dart';
import 'package:test/test.dart';

void main() {
  group('ISO 8601 tests', () {
    final idtUtc = IsoDateTime.utc(2023);
    final idt = IsoDateTime(2023);

    setUp(() {});

    test('Simple ctor test', () {
      // isUtc
      expect(idtUtc.isUtc, isTrue);
      expect(idt.isUtc, isFalse);
    });
  });
}
