import 'package:iso_date_time/iso_date_time.dart';
import 'package:test/test.dart';

void main() {
  group('ISO 8601 tests', () {
    setUp(() {});

    test('weeks of year', () {
      expect(IsoDateTime(2018, 1, 1).weekOfYear, 1); // Jan 1 is a Monday.
      expect(IsoDateTime(2018, 1, 7).weekOfYear, 1);
      expect(IsoDateTime(2018, 1, 8).weekOfYear, 2);
      expect(IsoDateTime(2019, 1, 1).weekOfYear, 1); // Jan 1 is a Tuesday.
      expect(IsoDateTime(2019, 1, 6).weekOfYear, 1);
      expect(IsoDateTime(2019, 1, 7).weekOfYear, 2);
      expect(IsoDateTime(2020, 1, 1).weekOfYear, 1); // Jan 1 is a Wednesday.
      expect(IsoDateTime(2020, 1, 5).weekOfYear, 1);
      expect(IsoDateTime(2020, 1, 6).weekOfYear, 2);
      expect(IsoDateTime(2015, 1, 1).weekOfYear, 1); // Jan 1 is a Thursday.
      expect(IsoDateTime(2015, 1, 4).weekOfYear, 1);
      expect(IsoDateTime(2015, 1, 5).weekOfYear, 2);

      expect(IsoDateTime(2021, 1, 4).weekOfYear, 1); // Jan 1 is a Friday.
      expect(IsoDateTime(2021, 1, 10).weekOfYear, 1);
      expect(IsoDateTime(2021, 1, 11).weekOfYear, 2);

      expect(IsoDateTime(2022, 1, 3).weekOfYear, 1); // Jan 1 is a Saturday.
      expect(IsoDateTime(2022, 1, 9).weekOfYear, 1);
      expect(IsoDateTime(2022, 1, 10).weekOfYear, 2);

      expect(IsoDateTime(2023, 1, 2).weekOfYear, 1); // Jan 1 is a Sunday.
      expect(IsoDateTime(2023, 1, 8).weekOfYear, 1);
      expect(IsoDateTime(2023, 1, 9).weekOfYear, 2);

      // 2022 had 52 weeks. January 1, 2023 was a Sunday, so it was in week 52.
      expect(IsoDateTime(2023, 1, 1).weekOfYear, 52);

      // 2015 started on a Thursday, so the first three days of 2016 are in week 53 of the previous year.
      expect(IsoDateTime(2016, 1, 1).weekOfYear, 53);
      expect(IsoDateTime(2016, 1, 2).weekOfYear, 53);
      expect(IsoDateTime(2016, 1, 3).weekOfYear, 53);
      expect(IsoDateTime(2016, 1, 4).weekOfYear, 1);

      // 2020 was a leap year that started on a Wednesday, so it had 53 weeks.
      expect(IsoDateTime(2021, 1, 1).weekOfYear, 53);
      expect(IsoDateTime(2021, 1, 2).weekOfYear, 53);
      expect(IsoDateTime(2021, 1, 3).weekOfYear, 53);
      expect(IsoDateTime(2021, 1, 4).weekOfYear, 1);
    });
  });
}
