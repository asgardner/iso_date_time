import 'package:iso_date_time/iso_date_time.dart';

void main() {
  var idt = IsoDateTime(2023, 6, 8);
  print(idt.toIso8601String()); // Will print 2023-06-08T00:00:00.000
  print(idt.dayOfYear); // 159
  print(idt.weekOfYear); // 23
}
