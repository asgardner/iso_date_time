<!-- 
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages). 

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages). 
-->

This package extends the built-in `DateTime` class with `dayOfYear` and `weekOfYear`
properties that conform to the ISO 8601 standard.

## Features

`DateTime` has six constructors. Each has an equivalent in `IsoDateTime`:
- `IsoDateTime`
- `IsoDateTime.fromMicroSecondsSinceEpoch`
- `IsoDateTime.fromMillisecondSinceEpoch`
- `IsoDateTime.now`
- `IsoDateTime.timestamp`
- `IsoDateTime.utc`

## Getting started

`IsoDateTime` should be a drop-in replacement in your code for `DateTime`, with the
addition of the `dayOfYear` and `weekOfYear` properties.

## Usage

```dart
var idt = IsoDateTime(2023, 6, 8);
print(idt.toIso8601String()); // 2023-06-08T00:00:00.000
print(idt.dayOfYear); // 159
print(idt.weekOfYear); // 23
```

## Additional information

For help, contact Andrew Gardner, agardner@arizona.edu. This package
was developed as an experiment with Dart, but I hope it's helpful.

Copyright (c) 2023 The Arizona Board of Regents on behalf of the University of Arizona.

![GPL v3 logo](https://www.gnu.org/graphics/gplv3-127x51.png)

This file is part of iso_date_time.

iso_date_time is free software: you can redistribute it and/or modify it 
under the terms of the GNU General Public License as published by the Free 
Software Foundation, either version 3 of the License, or (at your option) 
any later version.

iso_date_time is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
iso_date_time. If not, see <https://www.gnu.org/licenses/>.

